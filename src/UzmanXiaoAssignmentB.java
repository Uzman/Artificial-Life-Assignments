import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 * Created by oguz on 05/05/16.
 */
public class UzmanXiaoAssignmentB {

    private static char fullChar = 9608;

    public static void main(String[] args) {
        System.out.println("We use Conway's Game Of Life");

        Scanner s = new Scanner(System.in);
        System.out.println("Enter number of steps:");
        
        int stepCount = s.nextInt();

        int width = 101;
        int height = 82;
        //Initialize the topolgy
        boolean[][] topology = new boolean[width][height];

        int middleWidth = (width + 1) / 2;
        int middleHeight = (height + 1) / 2;

        System.out.println("Enter which pattern you would like to start with (enter its number) :");
        System.out.println("1. Blinker");
        System.out.println("2. Glider");
        System.out.println("3. R-Pentomino");
        System.out.println("4. Gosper's Glider Gun");
        System.out.println("5. Beacon");


        int selection = s.nextInt();
        if (selection < 1 || selection > 5) {
            System.out.println("Invalid input, terminating");
        }
        boolean[][] positives;

        switch (selection) {
            case 1:
                topology[(middleWidth + 1 + width) % width ][middleHeight] =
                        topology[(middleWidth + width) % width][middleHeight] =
                                topology[(middleWidth - 1 + width)% width ][middleHeight] = true;
                break;
            case 2:
                topology[(middleWidth -2 + width) % width ][middleHeight] =
                        topology[(middleWidth -1 + width) % width ][middleHeight] =
                                topology[(middleWidth  + width) % width ][middleHeight] =
                                        topology[(middleWidth  + width) % width][middleHeight+1] =
                                                topology[(middleWidth -1 + width) % width ][middleHeight + 2] =
                                                        true;
                break;
            case 3:
                topology[(middleWidth + width) % width][middleHeight -1] =
                        topology[(middleWidth + width + 1) % width][middleHeight -1] = true;

                topology[(middleWidth + width -1) % width ][middleHeight ] =
                        topology[(middleWidth + width ) % width][middleHeight ] = true;

                topology[(middleWidth + width ) % width][middleHeight + 1] = true;

                break;
            case 4:
                break;
            case 5:
                break;
        }

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("1st time step-------------------------------------------------------------------------\n");

        for (int t = 0; t < stepCount; t++){

            getTopologyString(width, height, topology, stringBuilder);
            stringBuilder.append(  "\n");
            stringBuilder.append(t+2);
            stringBuilder.append("th time step--------------------------------------------------------\n");

            boolean[][] nextState = new boolean[width][height];

            for(int i = 0; i < width; i++){
                for (int j = 0; j < height; j++){
                    //Check surrounding cells
                    int neighborAliveCount = 0;
                    for (int n = -1; n < 2; n++){
                        for(int m = -1; m < 2; m++){
                            if(n == 0 && m == 0){

                            }else {
                                if(topology[(i+width+n)%width][(j+height+m)%height])
                                    neighborAliveCount++;
                            }
                        }
                    }
                    if(topology[i][j]){
                        //Cell is alive

                        switch (neighborAliveCount){
                            case 0:
                            case 1:
                                //Death from loneliness
                                nextState[i][j] = false;// Redundant but we put it anyway.
                                break;
                            case 2:
                            case 3:
                                nextState[i][j] = true; // Survive.
                                break;
                            default:// greater than 3
                                //Death from overcrowding
                                nextState[i][j] = false; // Also redundant.
                        }

                    }else {
                        //Cell is dead
                        switch (neighborAliveCount){
                            case 3:
                                //Birth
                                nextState[i][j] = true;
                                break;
                            default:
                                //Still no life
                                nextState[i][j] = false;
                        }
                    }
                }
            }

            topology = nextState;

        }

        getTopologyString(width, height, topology, stringBuilder);

        //System.out.print(stringBuilder.toString());

        try {
            PrintWriter out = new PrintWriter("filename.txt");
            out.print(stringBuilder.toString());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    private static void getTopologyString(int width, int height, boolean[][] topology, StringBuilder stringBuilder){
        for(int i = 0; i < height; i++){
            for(int j = 0; j < width; j++){
                stringBuilder.append(  topology[j][i] ? fullChar : " ");
            }
            stringBuilder.append("\n");
        }
    }
}
